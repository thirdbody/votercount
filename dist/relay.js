"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const getCSV_1 = __importDefault(require("./getCSV"));
async function grepCSV(drive_file_id, toGet) {
    const parsed = await getCSV_1.default(drive_file_id);
    console.log("- - Parsing obtained CSV file...");
    return parsed.find((o) => o[0] === String(toGet));
}
exports.grepCSV = grepCSV;
async function verifyUser(id, first) {
    const user = await grepCSV(process.env.ROLL_FILE_ID, id);
    console.log("- Got parsed CSV file. Checking if first is valid...");
    try {
        return user[2].toLowerCase() === first.toLowerCase();
    }
    catch (_a) {
        return false;
    }
}
exports.verifyUser = verifyUser;
