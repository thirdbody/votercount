"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const auth_1 = __importDefault(require("./auth"));
const papaparse_1 = require("papaparse");
const node_fetch_1 = __importDefault(require("node-fetch"));
async function csv2arr(s) {
    return new Promise((resolve, reject) => {
        papaparse_1.parse(s, {
            complete: results => {
                console.log("- - - Parsed a CSV string!");
                resolve(results.data);
            },
            error: error => {
                console.log("- - - Failed to parse a csv string");
                reject({
                    status: error,
                    statusText: error.message
                });
            }
        });
    });
}
async function getCSV(ofFileID) {
    console.log(`- - - Processing file ${ofFileID}`);
    const creds = await auth_1.default();
    const access = (await creds.getAccessToken()).token;
    const res = await node_fetch_1.default(`https://www.googleapis.com/drive/v3/files/${ofFileID}/export?mimeType=text%2Fcsv`, {
        headers: {
            "Authorization": `Bearer ${access}`,
            "Accept": "application/json"
        }
    });
    console.log(`Got response ${res}`);
    return await csv2arr(await res.text());
}
exports.default = getCSV;
