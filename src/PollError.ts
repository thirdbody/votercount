export class PollAlreadyExistsError extends Error {
  constructor(msg: string) {
    super(msg);
    this.name = "PollAlreadyExistsError";
  }
}
