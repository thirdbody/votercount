import * as fs from "fs";
import { PollAlreadyExistsError } from "./PollError";

export class PollOption {
  public count: number;
  constructor(public name: string) {
    this.count = 0;
  }
}

export default class Poll {
  public options: PollOption[];

  constructor(public name: string, options: string[]) {
    if (fs.existsSync(`${this.name}.poll`)) {
      throw new PollAlreadyExistsError("Poll with that name already exists!");
    }
    this.options = options.map(option => new PollOption(option));
  }

  public save(): void {
    fs.writeFileSync(`${this.name}.poll`, JSON.stringify(this));
  }

  get winner(): string {
    const sorted = [...this.options];
    sorted.sort(
      (a: PollOption, b: PollOption): number => {
        if (a.count < b.count) {
          return -1;
        }
        if (a.count > b.count) {
          return 1;
        }
        return 0;
      }
    );

    return sorted[0].name;
  }
}
